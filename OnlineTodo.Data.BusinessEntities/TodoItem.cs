﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTodo.Business.Entities
{
    public class TodoItem : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Complete { get; set; }
    }
}
