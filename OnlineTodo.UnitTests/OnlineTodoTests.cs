using Microsoft.VisualStudio.TestTools.UnitTesting;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.UOW;
using OnlineTodo.Data.UOW.Interfaces;
using System.Linq;

namespace OnlineTodo.UnitTests
{
    [TestClass]
    public class OnlineTodoTests
    {
        IUnitOfWork UOW = new PersistenceUOW();

        [TestMethod]
        [TestCategory("RepositoryTests")]
        public void TestRepositoryTodoItemGet()
        {
           var listOfTodoItems = UOW.GetRepository<TodoItem>().All();

            Assert.IsNotNull(listOfTodoItems, "Repository Retrieval failed, the list of items was null");
        }

        [TestMethod]
        [TestCategory("RepositoryTests")]
        public void TestRepositoryAddItem()
        {
            UOW.GetRepository<TodoItem>().Add(new TodoItem()
                {
                    Complete = false,
                    Description ="Test Record",
                    CreatedDate =new System.DateTime(2019,1,1),
                    DueDate = new System.DateTime(2019,1,1),Id = 999,
                    UserName ="Test"
                }
            );

            //The journal is not updated fast enough
            //to reflect changes, as a future proof
            //the key would be to setup an event trigger
            //or perhaps observer pattern in order to pick up
            //latest db changes
            System.Threading.Thread.Sleep(500);

            var listOfTodoItems = UOW.GetRepository<TodoItem>().All();

            Assert.IsTrue(listOfTodoItems.Where(entity=>entity.Id == 999).Count() > 0, "Repository Add failed");
        }

        [TestMethod]
        [TestCategory("RepositoryTests")]
        public void TestRepositoryClear()
        {
            UOW.GetRepository<TodoItem>().Clear();

            //The journal is not updated fast enough
            //to reflect changes, as a future proof
            //the key would be to setup an event trigger
            //or perhaps observer pattern in order to pick up
            //latest db changes
            System.Threading.Thread.Sleep(500);

            var listOfTodoItems = UOW.GetRepository<TodoItem>().All();

            Assert.IsTrue(listOfTodoItems.Count() == 0, "DB was not cleared, there are still items in the collection");
        }
    }
}
