﻿using OnlineTodo.Data.DataAccess.Context;
using OnlineTodo.Data.Repository;
using OnlineTodo.Data.Repository.Interfaces;
using OnlineTodo.Data.UOW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTodo.Data.UOW
{
    public class PersistenceUOW : IUnitOfWork 
    {
        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            if (RepositoryLibrary.Instance.RepositoryList.Keys.Contains(typeof(TEntity)))
                return RepositoryLibrary.Instance.RepositoryList[typeof(TEntity)] as IRepository<TEntity>;
            
            //TODO::UNITY INJECTION? config DI to choose a repo type
            var repository = new MemStateRepository<TEntity>();

            RepositoryLibrary.Instance.RepositoryList.Add(typeof(TEntity), repository);
            return repository;
        }
    }
}
