﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineTodo.Data.UOW
{
    public sealed class RepositoryLibrary
    {
        private static RepositoryLibrary instance = null;
        private static readonly object padlock = new object();
        public Dictionary<Type, object> RepositoryList;

        RepositoryLibrary()
        {
            RepositoryList = new Dictionary<Type, object>();
        }

        public static RepositoryLibrary Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new RepositoryLibrary();
                    }
                    return instance;
                }
            }
        }
    }
}
