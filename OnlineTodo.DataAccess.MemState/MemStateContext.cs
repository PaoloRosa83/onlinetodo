﻿using OnlineTodo.Business.Entities;
using System;
using System.Collections.Generic;

namespace OnlineTodo.Data.DataAccess.Context
{
    public class MemStateContext : DBContext
    {
        public MemStateContext()
        {
            Init();
        }
        
        public ICollection<TodoItem> Todo { get; set; }

        public void Init()
        {
            if (Todo == null)
            {
                Todo = new List<TodoItem>();
            }
        }
    }
}
