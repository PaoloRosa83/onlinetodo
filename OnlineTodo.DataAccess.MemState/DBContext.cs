﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineTodo.Data.DataAccess.Context
{
    public interface DBContext
    {
        void Init();
    }
}
