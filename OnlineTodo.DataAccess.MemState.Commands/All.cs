﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class All<TEntity> : Memstate.Query<MemStateContext, IEnumerable<TEntity>>
    {
       
        public override IEnumerable<TEntity> Execute(MemStateContext model)
        {
            //compile the linq expression and invoke this against the object
            //to attempt to return the find data for repository purposes 

            var props = from p in model.GetType().GetProperties()
                        where p.PropertyType == typeof(TEntity) ||
                              p.PropertyType == typeof(ICollection<TEntity>)
                       select new { Property = p };

            var propertyInfo = props.FirstOrDefault();
            
            var result = from x in ((ICollection<TEntity>)propertyInfo.Property.GetValue(model))
                          select x;
           
            if (result is IEnumerable<TEntity>)
            {
                return (IEnumerable<TEntity>)result;
            }
            try
            {
                return (IEnumerable<TEntity>)Convert.ChangeType(result, typeof(IEnumerable<TEntity>));
            }
            catch (InvalidCastException)
            {
                //TODO: IMPLEMENT LOGGER FOR ERRORS
                return default(IEnumerable<TEntity>);
            }
            
        }
    }
}
