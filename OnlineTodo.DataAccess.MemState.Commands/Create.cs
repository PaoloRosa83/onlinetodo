﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class Create<TEntity> : Memstate.Command<MemStateContext>
    {
        //Encapsulate the Predicate for use in the execute
        //marked as read only to prevent predicate changes outside of the repository
        private TEntity internalModel;
        
        //Command Constructor to pass our linq query
        public Create(TEntity Model)
        {
            internalModel = Model;
        }
        
        public override void Execute(MemStateContext model)
        {
            if (internalModel != null)
            {
                var props = from p in model.GetType().GetProperties()
                            where p.PropertyType == typeof(TEntity) ||
                                  p.PropertyType == typeof(ICollection<TEntity>)
                            select new { Property = p };

                var propertyInfo = props.FirstOrDefault();

                var collection = (ICollection<TEntity>)propertyInfo.Property.GetValue(model);
                collection.Add(internalModel);
            }
        }
    }
}
