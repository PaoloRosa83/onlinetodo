﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class Clear<TEntity> : Memstate.Command<MemStateContext>
    {
        public override void Execute(MemStateContext model)
        {
            var props = from p in model.GetType().GetProperties()
                        where p.PropertyType == typeof(TEntity) ||
                              p.PropertyType == typeof(ICollection<TEntity>)
                        select new { Property = p };

            var propertyInfo = props.FirstOrDefault();

            var collection = (ICollection<TEntity>)propertyInfo.Property.GetValue(model);
            collection.Clear();
        }
    }
}
