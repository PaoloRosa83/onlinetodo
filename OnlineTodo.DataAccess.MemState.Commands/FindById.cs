﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class FindById<TEntity> : Memstate.Query<MemStateContext, TEntity>
    {
        //Encapsulate the Predicate for use in the execute
        //marked as read only to prevent predicate changes outside of the repository
        private long internalId;

        //Command Constructor to pass our linq query
        public FindById(long Id)
        {
            internalId = Id;
        }
        
        public override TEntity Execute(MemStateContext model)
        {
            //compile the linq expression and invoke this against the object
            //to attempt to return the find data for repository purposes 

            var props = from p in model.GetType().GetProperties()
                        where p.PropertyType == typeof(TEntity) ||
                              p.PropertyType == typeof(ICollection<TEntity>)
                        select new { Property = p };

            var propertyInfo = props.FirstOrDefault();


            var result = from x in ((List<TEntity>)propertyInfo.Property.GetValue(model)).Filter("Id", internalId.ToString())
                         select x;


            if (result is IEnumerable<IEntity>)
            {
                return ((IEnumerable<TEntity>)result).FirstOrDefault();
            }
            try
            {
                return ((IEnumerable<TEntity>)Convert.ChangeType(result, typeof(IEnumerable<TEntity>))).FirstOrDefault();
            }
            catch (InvalidCastException)
            {
                //TODO: IMPLEMENT LOGGER FOR ERRORS
                return default(TEntity);
            }
            
        }
    }
}
