﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Internal;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class Update<TEntity> : Memstate.Command<MemStateContext>
    {
        //Encapsulate the Predicate for use in the execute
        //marked as read only to prevent predicate changes outside of the repository
        private TEntity internalModel;
        private int internalRecordId;
        
        //Command Constructor to pass our linq query
        public Update(TEntity Model, int recordId)
        {
            internalModel = Model;
            internalRecordId = recordId;
        }
        
        public override void Execute(MemStateContext model)
        {
            if (internalModel != null)
            {
                var props = from p in model.GetType().GetProperties()
                            where p.PropertyType == typeof(TEntity) ||
                                  p.PropertyType == typeof(ICollection<TEntity>)
                            select new { Property = p };

                var propertyInfo = props.FirstOrDefault();


                var result = from x in ((List<TEntity>)propertyInfo.Property.GetValue(model)).Filter("Id", internalRecordId.ToString())
                             select x;

                var collection = (ICollection<TEntity>)propertyInfo.Property.GetValue(model);

                if (result.Count() > 0)
                {

                    if (collection.IndexOf(result.FirstOrDefault()) != -1)
                    {
                        //remove from the list
                        collection.Remove(result.FirstOrDefault());
                    }

                    //add a replacement
                    collection.Add(internalModel);
                }
            }
        }
    }

    static class Extensions
    {
        public static List<T> Filter<T>
            (this List<T> source, string columnName,
             string compValue)
        {
            ParameterExpression parameter = Expression.Parameter(typeof(T), "x");
            Expression property = Expression.Property(parameter, columnName);
            Expression constant = Expression.Constant(Convert.ToInt32(compValue));
            Expression equality = Expression.Equal(property, constant);
            Expression<Func<T, bool>> predicate =
                Expression.Lambda<Func<T, bool>>(equality, parameter);

            Func<T, bool> compiled = predicate.Compile();
            return source.Where(compiled).ToList();
        }
    }
}
