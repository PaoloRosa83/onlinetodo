﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

namespace OnlineTodo.DataAccess.MemState.Commands
{
    public class Find<TEntity> : Memstate.Query<MemStateContext, IEnumerable<TEntity>>
    {
        //Encapsulate the Predicate for use in the execute
        //marked as read only to prevent predicate changes outside of the repository
        private readonly Expression<Func<TEntity, bool>> Predicate;

        //Command Constructor to pass our linq query
        public Find(Expression<Func<TEntity, bool>> query)
        {
            Predicate = query;
        }
        
        public override IEnumerable<TEntity> Execute(MemStateContext model)
        {
            //compile the linq expression and invoke this against the object
            //to attempt to return the find data for repository purposes 

            var props = from p in model.GetType().GetProperties()
                        where p.PropertyType == typeof(TEntity) ||
                              p.PropertyType == typeof(ICollection<TEntity>)
                       select new { Property = p };

            var propertyInfo = props.FirstOrDefault();
            
            var result = from x in ((ICollection<TEntity>)propertyInfo.Property.GetValue(model))
                  .Where(Predicate.Compile()) //here compile your clausuly
                          select x;
           
            if (result is IEnumerable<TEntity>)
            {
                return (IEnumerable<TEntity>)result;
            }
            try
            {
                return (IEnumerable<TEntity>)Convert.ChangeType(result, typeof(IEnumerable<TEntity>));
            }
            catch (InvalidCastException)
            {
                //TODO: IMPLEMENT LOGGER FOR ERRORS
                return default(IEnumerable<TEntity>);
            }
            
        }
    }
}
