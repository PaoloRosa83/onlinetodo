﻿using Memstate;
using OnlineTodo.Business.Entities;
using OnlineTodo.Data.DataAccess.Context;

using OnlineTodo.Data.Repository.Interfaces;
using OnlineTodo.DataAccess.MemState.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace OnlineTodo.Data.Repository
{
    /// <summary>
    /// This is our repository for interactions when using Memstate
    /// more information on Memstate can be found here:
    /// https://github.com/DevrexLabs/memstate/tree/master/src/Memstate.Docs.GettingStarted/QuickStart
    /// The repository pattern is being leveraged and combined with Memstate
    /// to provide a seamless data layer to either Redis or MemState as our
    /// Persistence technology of choice
    /// Both Redis and Memstate allow for in memory databases with transaction logging
    /// for persistence continuity should NVDIMM not be in use by the server.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MemStateRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private Engine<MemStateContext> engine;

        public MemStateRepository()
        {
            engine = Engine.Start<MemStateContext>().Result;
        }
        
        public void Add(TEntity entity)
        {
            engine.Execute(new Create<TEntity>(entity));
        }

        public IEnumerable<TEntity> All()
        {
            return engine.Execute(new All<TEntity>()).Result;
        }

        public void Delete(TEntity entity)
        {
            engine.Execute(new Delete<TEntity>(entity));
        }

        public void Clear()
        {
            engine.Execute(new Clear<TEntity>());
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
           
           return engine.Execute(new Find<TEntity>(predicate)).Result;
        }

        public TEntity GetById(long id)
        {
            return engine.Execute(new FindById<TEntity>(id)).Result;
        }

        public void Update(TEntity entity,int Id)
        {
            engine.Execute(new Update<TEntity>((TEntity)entity,Id));
        }
    }
}
