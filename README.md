## Chosen Approach

The Design goal when putting the project together was to retain a compartmentalized / abstracted layer based solution.

The underlying technologies are as follows:

1. The front end makes use of AngularJS as the Scripting library and bootstrap as the UI/UX Library.
	1.1 Dual way binding reduces the clutter of lengthy Jquery code, angular has a write less approach.
	1.2 angular allows for neat segregation of code such as being able to inject a common service for data CRUD into a controller.
	1.3 Minimal requirement in manipulating the DOM leading to better performance overall
	1.4 Bootstrap facilitates easier UI/UX design and enables easier design for responsive website requirements
2. The front end makes use of MVC (Core Framework 2.1) for security / controller->Service communication
	2.1 MVC is a well recognised pattern running for many years in the dev community and has alot of community support
	2.2 I had to this point not embraced the Core Libraries since core 1 was released, so felt there was an opportunity to experiment and
		work around a fresher technology and examine the breaking changes created in core 2.
	2.3 Core 2.1 has a great plethora of web standards both old and new such as claims based authentication systems and is highly configurable
		through its startup interface.
3. The Service layer makes use of Core Framework 2.1 WEBAPI (as a Resful Service)
	3.1 A Restful service was decided upon due to it's simplistic clean action/verb structure
		considering the primary focus of the TodoList would be a simple CRUD interface
		the protocol is also more light weight than say a WCF Soap service, which was another point for performance gain
		lastly, with the use of bootstrap, the solution could go responsive for mobile development and light transfer packets
		would be a positive.
	3.2 The abstraction within the solution allows for easy snap in of a WCF service should the technology of choice be changed.
		via the APIClient functionality in the UI Project.
	3.3 ASP Core is also Microsofts more cross platform implementation - as a result the service and UI have greater options
		for hosting - as with the UI I have also not embraced the Core Libraries since core 1, so felt there was an opportunity to experiment and
		work around the breaking changes in core 2.
4. The Database is composed of a third party open source technology known as MemState
	4.1 Memstate is still in early access and as such is a work in progress.
	4.2 Memstate allows for Read/Write access to an in memory database.
	4.3 Memstate has a journalling feature which allows for disk writes to circumnavigate 
		loss of data in case of power loss in non NVRAMM server environments.
	4.4 Memstate was chosen over the likes of other Databases such as SQLLite for it's ability to not only run in memory but recover gracefully through journalling.
	4.5 Another choice of database would be technology such as Redis - with it's myriad of .Net Core providers,
		however I decided to take on the challenge of rolling my own "Provider" for Memstate - which does not exist yet.
	4.6 Entity Framework may have been a simpler choice due to it's library of providers, however there are schools of thought around
		its performance and generated queries. 

## Design Patterns and Practices

	1. The overall architecture of the system is an N-Tier Architecture consisting of:
		1.1 Presentation Layer
		1.2 Business Layer
		1.3 Service Layer
		1.4 Data Layer
		1.5 Persistence Layer
	2. The presentation layer is designed as follows:
		2.1 Implementation of a traditional Model View Controller - however I have broken out
			of the razor model implementation (except in the login portion where I kept the traditional mechanism)
			in favour of a POJO mechanism to aid in the angular.js implementation for get/post and model binding.
		2.2 A Factory pattern has been used that makes use of a ClientAPI blue print with the standard rest verbs
			which instantiates a concrete APIClient (in this case TodoItemAPI) abstracting the communication logic and allowing
			simple calls from the controller to the REST Service.
	3. The Services Layer
		3.1 The service layer is a RESTFUL Service 
		3.2 Each controller makes use of a base class that behind the scenes implements a unit of work pattern
			in this case the unit of work is the persistence unit, this unit of work houses a repository for
			each entity type and allows executions to be run against any given repository (with more time I would like 
			to have had the repository type injected in via config for greater flexibility)
	4. Data Layer
		4.1 I have made use of a Repository Pattern in the data layer as a mechanism to interchange data access layers
			should the architecture require IE: A SQLite Repository or an entity framework repository, the functionality
			is built generically with the action verbs, so as a result regardless of technology, the repository should be
			mostly plug and play and the unit of work would need to know which repository the caller (webapi controller) is 
			pulling from.
	5. Persistence Layer
		5.1 Memstate has a bit of a strange architecture, the DBContext is simply a list of list properties derived from business objects
			within the environment
		5.2 Memstate also makes use of queries and command implementations in it's "engine", these are bite sized "units of work" that
			the engine instantiates and executes
		5.3 as a result I have engineered the strongly typed queries and commands into a more generic set of commands and have implemented
			the engine implemnentation logic into the repository pattern class.
			
The main concept behind my design approach was to make the solution layers fully abstract of each other and allowing for generic injection of each
layer in order to allow for flexibility in terms of swapping out layers be it from presentation all the way through to the persistence layer.

## With More Time

 1. Validation currently is fairly minimal
 2. Implement security mechanisms into the resful service as currently any user can access the endpoints exposed.
 3. Angular toast notifications for a fresh UI Experience when deleting or adding a todo item
 4. The memstate commands could have some refactoring done and logic could be broken out to helper classes
 5. Exception handling is minimal currently, I would likely have made use of a third party library such as Log4net and 
 	injected this in for error handling notifications
 6. Bolt Swagger UI onto the web api layer to be combined with OData for easier webapi definitions 
 7. Bolt OData onto the web api layer for more flexible data retrieval logic, such as pagination / greater filters etc (Scability Reasons)
 8. The Persistence Unit of work currently is hardcoded to use the MemStateRepository, ideally I would like to have this injected
 	with a library like Unity to allow replacement of the repository with any other concrete repository implementation

## Notes

Although the requirement was relatively simple and could be accomplished with a traditional mvc 5 -> WCF Service -> Entity Framework -> In Memory DB,
I decided to try my and experiment with newer technologies - I also tried to show a few different types of patterns and practices
from the Factory pattern in the clientAPI to the repository and Unit of work patterns in the service.

Memstate as a database library although powerful by nature has a few limitations and by design tries to shy away from need for abstractions
Memstate makes use of a concept they call queries and commands whereby each query and command is typically hardcoded and strongly typed against the 
database context in order to retrieve data - for example they create a query called GetTop10Users - which the implementation contains a List<User>.Where
since I decided to make use of the repository pattern in order to abstract out the data access as a result I had to bend the command and query interfaces
of Memstate to allow more generic functionality - as a result they became fairly reflection heavy (the implementation in itself became pretty challenging
as I had to make use of Expression trees for dynamic logic (no casts to base classes!)) - reflection adds to performance degredation, but 
the unit tests indicate an access time of milliseconds.

I do apologise for the delay, however due to unforseen circumstances on Sunday and priority 1 issues today I was only able to complete and upload the
rest of the code today.