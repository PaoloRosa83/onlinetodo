﻿using OnlineTodo.Business.Entities;
using System;

namespace OnlineTodo.Business.Helpers
{
    public static class TodoItemHelper
    {
        /// <summary>
        /// Used as a business layer to add augmented info to the todoitem
        /// that the front end or any other front end could be replaced whilst
        /// keeping the same instantiation logic 
        /// </summary>
        /// <param name="todoItem"></param>
        /// <param name="userName"></param>
        public static void AugmentTodoItem(TodoItem todoItem,string userName)
        {
            todoItem.Complete = false;
            todoItem.CreatedDate = DateTime.Now;
            todoItem.UserName = userName;
            todoItem.Id = Math.Abs(Guid.NewGuid().GetHashCode());
        }
    }
}
