﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace OnlineTodo.Data.Repository.Interfaces
{
    /// <summary>
    /// This is our repository contract
    /// this defines what any of our repository classes (Our communication between server and DB)
    /// this ensures that Whether by Memstate/sqlite or redis, our data layer interaction
    /// will never need to be changed in our other layers
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> 
        where TEntity : class
    {
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Clear();
        void Update(TEntity entity, int Id);
        TEntity GetById(long id);
        IEnumerable<TEntity> All();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
