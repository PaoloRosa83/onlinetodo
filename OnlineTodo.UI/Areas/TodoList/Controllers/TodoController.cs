﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OnlineTodo.Business.Entities;
using OnlineTodo.Business.Helpers;
using OnlineTodo.UI.Models;

namespace OnlineTodo.UI.Areas.TodoList.Controllers
{
    [Area("TodoList")]
    [Route("Todo/[controller]"), Produces("application/json")]
    [Authorize]
    public class TodoController : Controller
    {
        private readonly IOptions<SettingsModel> appSettings; 

        public TodoController(IOptions<SettingsModel> app)
        {
            appSettings = app;
            ApplicationSettings.WebApiUrl = appSettings.Value.WebApiBaseUrl;
        }

        /// <summary>
        /// The Main Page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View();
        }

        /// <summary>
        /// Refreshes the todo list after any action
        /// we could technically for performance reasons
        /// manipulate the list HOWEVER if we refresh we
        /// are guaranteed that if for some unknown reason the user is adding 
        /// toddo items on 100 different machines, that the proper task list is shown
        /// </summary>
        /// <returns></returns>
        [Route("[action]")]
        [HttpGet]
        [ResponseCache(CacheProfileName = "CacheProfile")]
        
        public JsonResult RefreshTodoList()
        {
            var data = ApiClientFactory.Instance.GetTodoList().Result;

            return new JsonResult(data);
        }

        /// <summary>
        /// Adds a new item to the db
        /// </summary>
        /// <param name="todoItem"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpPost]
        public IActionResult AddTodoItem([FromBody] TodoItem todoItem)
        {
            //Move logic to a seperate business layer, controllers
            //should be light weight and business should handle the rest
            TodoItemHelper.AugmentTodoItem(todoItem, User.Identity.Name);

            ApiClientFactory.Instance.SaveTodo(todoItem);
            return Json(new { response = true });
        }

        /// <summary>
        /// Updates a record in the DB
        /// </summary>
        /// <param name="todoItem"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpPost]
        public IActionResult UpdateTodoItem([FromBody] TodoItem todoItem)
        {
            ApiClientFactory.Instance.PutTodo(todoItem);
            return Json(new { response = true });
        }

        /// <summary>
        /// Deletes an item from the list
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [Route("[action]")]
        [HttpPost]
        public IActionResult DeleteTodoItem([FromBody]int Id)
        {
            ApiClientFactory.Instance.DeleteTodo(Id);
            return Json(new { response = true });
        }

        /// <summary>
        /// Clears the todo list database
        /// </summary>
        /// <returns></returns>
        [Route("[action]")]
        [HttpPost]
        public IActionResult ClearTodoList()
        {
            ApiClientFactory.Instance.ClearTodo();
            return Json(new { response = true });
        }

    }
}