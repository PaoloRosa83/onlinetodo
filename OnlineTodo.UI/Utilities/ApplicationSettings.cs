﻿using OnlineTodo.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTodo.UI
{
    public static class ApplicationSettings
    {
        public static string WebApiUrl { get; set; }
    }
}
