﻿using OnlineTodo.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTodo.UI
{
    public partial class ApiClient
    {
        private Uri BuildRequestUri()
        {
            //Build the request URL using the todo
            //with more refactoring this uri could also be configured and injected in for greater generic functionality.
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
              "TodoItem"));

            return requestUrl;
        }

        /// <summary>
        /// Retrieve A list of Todo Items
        /// </summary>
        /// <returns></returns>
        public async Task<List<TodoItem>> GetTodoList()
        {
            return await GetAsync<List<TodoItem>>(BuildRequestUri());
        }

        /// <summary>
        /// Save a New Todo Item
        /// </summary>
        /// <param name="model">The todoitem Model</param>
        public async void SaveTodo(TodoItem model)
        {
            await PostAsync<TodoItem>(BuildRequestUri(), model);
        }

        /// <summary>
        /// Update a Todo Item
        /// </summary>
        /// <param name="model"></param>
        public async void PutTodo(TodoItem model)
        {
            await PutAsync<TodoItem,TodoItem>(BuildRequestUri(), model);
        }

        /// <summary>
        /// Update a Todo Item
        /// </summary>
        /// <param name="model"></param>
        public async void DeleteTodo(int id)
        {
            await DeleteAsync<TodoItem, string>(BuildRequestUri(), id.ToString());
        }

        /// <summary>
        /// Clears the Todo List
        /// </summary>
        /// <param name="model"></param>
        public async void ClearTodo()
        {
            await ClearAsync<TodoItem>(BuildRequestUri());
        }
    }
}
