﻿
app.controller('TodoItemCtrl', TodoItemCtrl);
TodoItemCtrl.$inject = ["$filter","$q", "$scope","TodoItemService"];

function TodoItemCtrl($filter,$q, $scope, TodoItemService) {
  
    $scope.TodoItem = { };

    $scope.AddTodoItemToList = function () {
        //TODO: Throw Notification to indicate complete... 

        //Also todo, set the system to do validation on simple textbox
       // if ($scope.TodoForm.$valid) {
            TodoItemService.AddNewTodoItem($scope.TodoItem).then(function (result) {
                
                //also Refresh TodoList
                $scope.RefreshTodoList();

            });
        //}
    }

    $scope.RefreshTodoList = function () {
        TodoItemService.RetrieveTodoList().then(function (result) {
            //Throw Notification to indicate complete... Refresh TodoList
            $scope.TodoList = result.data;
        });
    }
    
    //Update the Todo Item with a given Id
    $scope.UpdateTodoItem = function (Id,event) {

        var todoItem = $filter('filter')($scope.TodoList, { id: Id })[0];
        todoItem.complete = event.target.checked;

        TodoItemService.UpdateTodoItem(todoItem).then(function (result) {
            $scope.RefreshTodoList();
        });
    }

    $scope.DeleteTodoItem = function (Id) {
        TodoItemService.DeleteTodoItem(Id).then(function (result) {
            $scope.RefreshTodoList();
        });
    }

    //Clears the DB, at some stage implement a warning to prevent
    //further clears without being sure 
    $scope.ClearList = function () {
        TodoItemService.ClearTodoList().then(function (result) {
            $scope.RefreshTodoList();
        });
    }

    //Initial Refresh to fetch data
    $scope.RefreshTodoList();
};

app.service('TodoItemService', TodoItemService);
TodoItemService.$inject = ["$http"];


function TodoItemService($http) {
    this.RetrieveTodoList = function () {
        return $http.get(APP_PATH + 'Todo/Todo/RefreshTodoList');
    }

    this.AddNewTodoItem = function (todoItem) {
        return $http.post(APP_PATH + 'Todo/Todo/AddTodoItem/', todoItem);
    }

    this.UpdateTodoItem = function (todoItem) {
        return $http.post(APP_PATH + 'Todo/Todo/UpdateTodoItem/',todoItem );
    }

    this.DeleteTodoItem = function (Id) {
        return $http.post(APP_PATH + 'Todo/Todo/DeleteTodoItem/', Id );
    }

    this.ClearTodoList = function () {
        return $http.post(APP_PATH + 'Todo/Todo/ClearTodoList/');
    }
};
