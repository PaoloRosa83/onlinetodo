﻿var app = (function () {
    // Instantiate the app with the appropriate modules
    var application = angular.module('app', [])

    // Configures the http provider
    application.config(['$httpProvider', function ($httpProvider) {
        //Sets the header Content-Type to handle json
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8'
    }]);

    return application;
})();