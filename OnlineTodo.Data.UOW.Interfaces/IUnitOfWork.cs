﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OnlineTodo.Data.DataAccess.Context;
using OnlineTodo.Data.Repository.Interfaces;
using OnlineTodo.Business.Entities;

namespace OnlineTodo.Data.UOW.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> GetRepository<TEntity>() where TEntity : class;

    }

    public interface IUnitOfWork<TContext> : IUnitOfWork where TContext : class
    {
        TContext Context { get; }
    }
}
