﻿using OnlineTodo.Data.DataAccess.Context;
using OnlineTodo.Data.Repository.Interfaces;
using OnlineTodo.Data.UOW;
using OnlineTodo.Data.UOW.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineTodo.Service.Controllers
{
    public class BaseController 
    {
        internal IUnitOfWork UnitOfWork;
        
        public BaseController()
        {
            //create a persistence unit to preform DB Saves
            UnitOfWork = new PersistenceUOW();
        }
    }
}
