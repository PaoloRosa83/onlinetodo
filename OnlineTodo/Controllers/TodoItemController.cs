﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineTodo.Business.Entities;
using OnlineTodo.Service;
using OnlineTodo.Service.Controllers;

namespace OnlineTodo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemController : BaseController
    {
        // GET api/TodoItem
        [HttpGet]
        public ActionResult<IEnumerable<TodoItem>> Get()
        {
            var todoItemList = UnitOfWork.GetRepository<TodoItem>().All();
            return todoItemList.ToList();
        }

        // GET api/TodoItem/5
        [HttpGet("{id}")]
        public ActionResult<TodoItem> Get(int id)
        {
            var todoItem = UnitOfWork.GetRepository<TodoItem>().GetById(id);
            return todoItem;
        }

        // POST api/TodoItem
        [HttpPost]
        public void Post([FromBody] TodoItem value)
        {
            UnitOfWork.GetRepository<TodoItem>().Add(value);
        }

        // PUT api/TodoItem
        [HttpPut]
        public void Put([FromBody] TodoItem value)
        {
            UnitOfWork.GetRepository<TodoItem>().Update(value, value.Id);
        }

        // DELETE api/TodoItem/5
        [HttpDelete("{id}")]
        public void Delete(int? id)
        {
            if (id != null)
            {
                var todoItem = UnitOfWork.GetRepository<TodoItem>().GetById(Convert.ToInt32(id));
                UnitOfWork.GetRepository<TodoItem>().Delete(todoItem);
            }
        }
        // DELETE api/TodoItem/
        [HttpDelete]
        public void Delete()
        {
            //treat as a clear table
            UnitOfWork.GetRepository<TodoItem>().Clear();
        }


    }
}
